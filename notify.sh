#!/bin/sh

# Defaults
GOTIFY_TITLE=${GOTIFY_TITLE:-"$CI_REPO_NAME: $CI_BUILD_STATUS"}
GOTIFY_MESSAGE=${GOTIFY_MESSAGE:-"$CI_COMMIT_BRANCH: $CI_COMMIT_MESSAGE"}
GOTIFY_PRIORITY=${GOTIFY_PRIORITY:-"5"}

# Send data to the Gotify server.
/usr/bin/curl \
  "$GOTIFY_URL/message?token=$GOTIFY_APP_TOKEN" \
  -F "title=$GOTIFY_TITLE" \
  -F "message=$GOTIFY_MESSAGE" \
  -F "priority=$GOTIFY_PRIORITY"
