FROM alpine:latest
RUN apk add curl
COPY ./notify.sh /notify.sh
RUN chmod 700 /notify.sh
ENTRYPOINT ["sh", "/notify.sh"]
